import { Routes } from "./constants";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { ReportDetailsPage } from "./pages/ReportDetailsPage";
import { ReportsPage } from "./pages/ReportsPage";

const router = createBrowserRouter([
  {
    path: Routes.REPORTS_LIST,
    element: <ReportsPage />,
  },
  {
    path: Routes.REPORT_DETAIL,
    element: <ReportDetailsPage />,
  },
  {
    path: "*",
    element: <div>Not found</div>,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
