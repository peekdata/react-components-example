const Routes = {
  REPORTS_LIST: "/reports",
  REPORT_DETAIL: "/report/:id",
};

export default Routes;
